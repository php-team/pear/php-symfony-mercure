php-symfony-mercure (0.6.5-3) unstable; urgency=medium

  * Modernize PHPUnit syntax

 -- David Prévot <taffit@debian.org>  Mon, 17 Feb 2025 12:21:32 +0100

php-symfony-mercure (0.6.5-2) unstable; urgency=medium

  * Make provider functions static (PHPUnit 11 Fix) (Closes: #1070603)

 -- David Prévot <taffit@debian.org>  Sun, 19 May 2024 20:05:48 +0200

php-symfony-mercure (0.6.5-1) unstable; urgency=medium

  [ Christian Flothmann ]
  * explicitly mark nullable parameters as nullable

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Sat, 13 Apr 2024 14:49:03 +0200

php-symfony-mercure (0.6.4-3) unstable; urgency=medium

  * Drop Polyfill from autoload.php

 -- David Prévot <taffit@debian.org>  Sun, 10 Mar 2024 10:05:46 +0100

php-symfony-mercure (0.6.4-2) unstable; urgency=medium

  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Wed, 06 Mar 2024 16:38:51 +0100

php-symfony-mercure (0.6.4-1) unstable; urgency=medium

  [ Nicolas Grekas ]
  * Allow Symfony 7

  [ Robin Chalas ]
  * Allow contracts v4 (#103)

  [ Kévin Dunglas ]
  * docs: changelog for version 0.6.4

 -- David Prévot <taffit@debian.org>  Mon, 04 Dec 2023 07:54:56 +0100

php-symfony-mercure (0.6.3-1) unstable; urgency=medium

  [ Tomas Norkūnas ]
  * Allow lcobucci/jwt 5.0 (#97)

  [ David Prévot ]
  * Drop upstream signature check
  * Update standards version to 4.6.2, no changes needed.
  * Drop pkg-php-tools Build-Dependcy providing dh-sequence-phpcomposer

 -- David Prévot <taffit@debian.org>  Tue, 07 Mar 2023 09:00:26 +0100

php-symfony-mercure (0.6.2-1) unstable; urgency=medium

  [ chapterjason ]
  * fix: Set Content-Type header always to application/x-www-form-urlencoded

  [ Christophe VERGNE ]
  * Return result from messenger update handler for handler middleware

  [ Daniel West ]
  * Feat: Security: Allow subsribe and publish topics to be passed as null to
    token factory (#93)

  [ Kévin Dunglas ]
  * add changelog for version 0.6.2 (#94)

 -- David Prévot <taffit@debian.org>  Sat, 26 Nov 2022 18:39:09 +0100

php-symfony-mercure (0.6.1-3) unstable; urgency=medium

  * Drop php-symfony-polyfill-php80 build-dependency

 -- David Prévot <taffit@debian.org>  Wed, 15 Jun 2022 09:36:54 +0200

php-symfony-mercure (0.6.1-2) unstable; urgency=medium

  * Upload to unstable now that Symfony allows it
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sat, 28 May 2022 15:13:16 +0200

php-symfony-mercure (0.6.1-1) experimental; urgency=medium

  [ Kévin Dunglas ]
  * complete changelog for v0.6.1 (#77)

 -- David Prévot <taffit@debian.org>  Sat, 18 Dec 2021 08:44:53 -0400

php-symfony-mercure (0.6.0-1) experimental; urgency=medium

  [ Jérémy Derussé ]
  * Allow Symfony 6.0

  [ Kévin Dunglas ]
  * feat: add Twig helper generating hub URLs and setting authorization cookies
  * add missing entries in 0.6.0 changelog (#67)

  [ Nicolas Grekas ]
  * Allow contracts v3

  [ David Prévot ]
  * Update upstream signing key
  * Add new build-dependencies:
    - php-symfony-event-dispatcher
    - php-symfony-http-kernel
    - php-symfony-polyfill-php80
    - php-twig

 -- David Prévot <taffit@debian.org>  Fri, 19 Nov 2021 16:24:59 -0400

php-symfony-mercure (0.5.3-1) experimental; urgency=medium

  * Initial release (new symfony build-dependency)

 -- David Prévot <taffit@debian.org>  Fri, 08 Oct 2021 10:04:29 -0400
